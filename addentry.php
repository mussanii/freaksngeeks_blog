<?php 
error_reporting(0);
session_start();
require("config.php");

$db =mysqli_connect($dbhost, $dbuser, $dbpassword);
mysqli_select_db($db, $dbdatabase);

if(!isset($_SESSION['username'])){
    header("Location:" . $config_basedir);
}
if($_POST['submit']){
    $sql ="INSERT INTO entries(cat_id, dateposted, subject, body)
    VALUES(" . $_POST['cat'] . ", NOW(),'" .$_POST['subject'] . "', '" .
    $_POST['body'] . "');";
    
    mysqli_query($db, $sql);
    header("Location:" .$config_basedir);
        
}else{
    require("header.php");
}
?>
<h1>Add new entry</h1>
<form action="<?= $_SERVER['SCRIPT_NAME']?>" method="post">
<table>
<tr>
<td>Category</td>
<td>
<select name="cat">
<?php 
$catsql= "SELECT * FROM categories;";
$catres = mysqli_query($db,$catsql);

while($catrow = mysqli_fetch_assoc($catres)){
    echo "<option value='" .$catrow['id'] . "'>" . $catrow['cat'].
    "<option>";
    
}

?>
</select>
</td>
</tr>
<td>Subject</td>
<td><input type="text" name="subject"></td>
<tr>
<td>Body</td>
<td><textarea name="body" rows="10" cols="50"></textarea></td>
</tr>
<tr>
<td></td>
<td><input type="submit" name="submit" value="Add Entry!"></td>
</tr>

</table>

</form>
<?php 
require("footer.php");
?>
